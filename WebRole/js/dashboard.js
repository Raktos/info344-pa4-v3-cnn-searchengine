﻿"use strict";

angular.module('DashboardApp', [])
    .controller('DashboardController', function ($scope, $http, $interval) {
        $scope.status = 'unknown';
        $scope.targets = '';
        $scope.cpu = 'loading';
        $scope.memUsed = 'loading';
        $scope.memAvaliable = 'loading';
        $scope.webMemAvaliable = 'loading';
        $scope.queueLength = 'loading';
        $scope.tableLength = 'loading';
        $scope.last10 = [];
        $scope.trieBuilt = 'False';
        $scope.downloaded = 'False';
        $scope.trieSize = 'unknown';
        $scope.trieLastTitle = 'unknown';
        $scope.cacheSize = 'unknown';
        $scope.numSearches = 'unknown';

        $scope.start = function () {
            $scope.status = 'starting';
            $http.post('WebService.asmx/Start', { 'args': $scope.startArgs })
                .success(function () {
                    $scope.status = 'loading';
                });
        };

        $scope.stop = function () {
            $scope.status = 'stopping';
            $http({
                url: 'WebService.asmx/Stop',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function () {
                    $scope.status = 'stopped';
                });
        };

        $scope.clear = function () {
            $scope.status = 'clearing';
            $http({
                url: 'WebService.asmx/Clear',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function () {
                    $scope.status = 'cleared';
                });
        };

        $scope.getStatus = function () {
            $http({
                url: 'WebService.asmx/GetCrawlerStatus',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.status = data['d'];
                });
        };

        $scope.getTargets = function () {
            $http({
                url: 'WebService.asmx/GetCrawlerTargets',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.targets = data['d'];
                });
        };

        $scope.getCpu = function () {
            $http({
                url: 'WebService.asmx/GetCpuUsed',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.cpu = data['d'];
                });
        };

        $scope.getMemUsed = function () {
            $http({
                url: 'WebService.asmx/GetMemUsed',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.memUsed = data['d'];
                });
        };

        $scope.getMemAvaliable = function () {
            $http({
                url: 'WebService.asmx/GetMemAvaliable',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.memAvaliable = data['d'];
                });
        };

        $scope.getQueueLength = function () {
            $http({
                url: 'WebService.asmx/GetQueueLength',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.queueLength = data['d'];
                });
        };

        $scope.getTableLength = function () {
            $http({
                url: 'WebService.asmx/GetTableLength',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.tableLength = data['d'];
                });
        };

        $scope.getWebMemRemaining = function () {
            $http({
                url: 'WebService.asmx/GetWebRoleMemRemaining',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.webMemAvaliable = data['d'];
                });
        };

        $scope.getLast10 = function () {
            $http({
                url: 'WebService.asmx/GetLast10Crawled',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.last10 = JSON.parse(data['d']);
                });
        };
        $scope.downloadBlobs = function () {
            $scope.downloaded = "downloading...";
            $http({
                url: 'WebService.asmx/Download',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function () {
                    $scope.getDownloadStatus();
                });
        };

        $scope.buildTrie = function () {
            $scope.trieBuilt = "building...";
            $http({
                url: 'WebService.asmx/BuildTrie',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function () {
                    $scope.getTrieStatus();
                    $scope.getTrieSize();
                    $scope.getLastTrieInsert();
                });
        };

        $scope.getDownloadStatus = function () {
            $http({
                url: 'WebService.asmx/IsDownloaded',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.downloaded = data['d'];
                });
        };

        $scope.getTrieStatus = function () {
            $http({
                url: 'WebService.asmx/IsTrieBuilt',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.trieBuilt = data['d'];
                    $scope.getTrieSize();
                    $scope.getLastTrieInsert();
                });
        };

        $scope.getTrieSize = function () {
            if ($scope.trieBuilt == 'True') {
                $http({
                    url: 'WebService.asmx/GetTrieSize',
                    dataType: 'json',
                    method: 'POST',
                    data: '',
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                    .success(function (data) {
                        $scope.trieSize = data['d'];
                    });
            }
        };

        $scope.getLastTrieInsert = function () {
            if ($scope.trieBuilt == 'True') {
                $http({
                    url: 'WebService.asmx/GetLastTitle',
                    dataType: 'json',
                    method: 'POST',
                    data: '',
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                    .success(function (data) {
                        $scope.trieLastTitle = data['d'];
                    });
            }
        };

        $scope.getCacheSize = function () {
            $http({
                url: 'WebService.asmx/GetCacheSize',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.cacheSize = data['d'];
                });
        };

        $scope.clearCache = function () {
            $scope.cacheSize = 'clearing...';
            $http({
                url: 'WebService.asmx/ClearCache',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function () {
                    $scope.getCacheSize();
                });
        };

        $scope.getNumSearches = function () {
            $http({
                url: 'WebService.asmx/GetCacheSize',
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (data) {
                    $scope.numSearches = data['d'];
                });
        };


        $scope.getCpu();
        $scope.getMemAvaliable();
        $scope.getMemUsed();
        $scope.getQueueLength();
        $scope.getTableLength();
        $scope.getWebMemRemaining();
        $scope.getStatus();
        $scope.getTargets();
        $scope.getLast10();
        $scope.getDownloadStatus();
        $scope.getTrieStatus();
        $scope.getCacheSize();
        $scope.getNumSearches();

        $interval($scope.getCpu, 10000);
        $interval($scope.getMemAvaliable, 10000);
        $interval($scope.getMemUsed, 10000);
        $interval($scope.getQueueLength, 10000);
        $interval($scope.getTableLength, 10000);
        $interval($scope.getWebMemRemaining, 10000);
        $interval($scope.getStatus, 10000);
        $interval($scope.getTargets, 10000);
        $interval($scope.getLast10, 10000);
        $interval($scope.getTrieStatus, 10000);
        $interval($scope.getCacheSize, 10000);
        $interval($scope.getNumSearches, 10000);
    });