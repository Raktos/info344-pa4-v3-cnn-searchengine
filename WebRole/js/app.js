﻿"use strict";

angular.module('SearchApp', ['ui.bootstrap'])
    .controller('SearchController', function ($scope, $http) {
        $scope.suggestions = [];
        $scope.results = [];
        $scope.basketballResults = [];

        $scope.getSuggestions = function () {
            $http.post('WebService.asmx/Suggestion', { 'input': $scope.input })
                .success(function (data) {
                    $scope.suggestions = JSON.parse(data['d']);
                });
        };

        $scope.getResults = function () {
            $http.post('WebService.asmx/Search', { 'searchphrase': $scope.input })
                .success(function (data) {
                    $scope.results = JSON.parse(data['d']);
                });
        };

        $scope.getBasketballPlayer = function () {
            $scope.basketballResults = [];
            $http.jsonp('http://ec2-54-149-163-184.us-west-2.compute.amazonaws.com/search_jsonp.php?callback=JSON_CALLBACK&name=' + $scope.input)
                .success(function (data) {
                    $scope.basketballResults.push(data['players'][0]);
                })
                .error(function (err) {
                    //do nothing
                });
        };
    })
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    });