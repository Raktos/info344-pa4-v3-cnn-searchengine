﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using Info344ClassLibrary;
using System.Threading;

namespace WebRole
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        //Storage connection
        static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
        static CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
        static CloudBlobContainer container = blobClient.GetContainerReference("wikipedia");

        static StorageManager storageManager = new StorageManager(CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]));

        private static readonly int NUMBER_CRAWLER_THREADS = 5;

        static JavaScriptSerializer js = new JavaScriptSerializer();

        static Dictionary<string, List<URLTableRow>> cache = new Dictionary<string, List<URLTableRow>>();
        static Queue<string> cacheQueue = new Queue<string>();

        static bool downloaded = false;
        static uint numSearches = 0;

        ///////////////
        ///PA2 Stuff///
        ///////////////

        //The Trie that will be used for queries
        static TrieDualNode TitlesTrie = null;

        //Performance Counter
        static PerformanceCounter memProcessWebRole = new PerformanceCounter("Memory", "Available MBytes");

        //Queries agains the dataset in the Trie and returns autocomplete results
        //Paremeters: Takes a string of input
        //Returns: returns a list of strings as a result, ordered by # of pageviews
        //returns as a JSON object
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Suggestion(string input)
        {
            //format the input string to remove whitespace, inser the underscores stored in the Trie, and ensure lowercase
            input = input.ToLower();
            input = Regex.Replace(input, @"\s+", " ");
            input = input.Replace(' ', '_');

            //serialize the result as a JSON object
            return js.Serialize(TitlesTrie.query(input)).Replace('_', ' ');
        }

        //Downloads the raw data from blog storage
        //Parameters: none
        //Returns: returns the status of the download, true or false
        [WebMethod]
        public bool Download()
        {
            //get the filepath to be used
            string target = Path.GetTempPath() + "/";

            if (container.Exists())
            {
                foreach (IListBlobItem item in container.ListBlobs(null, false))
                {
                    //get the URL that will be downloaded
                    string[] url = item.Uri.ToString().Split('/');

                    //if we want this blob...
                    if (item.GetType() == typeof(CloudBlockBlob))
                    {
                        CloudBlockBlob blob = (CloudBlockBlob)item;

                        //save the blob locally to the filepath/filename
                        var fileStream = File.OpenWrite(target + url[url.Length - 1]);
                        blob.DownloadToStream(fileStream);
                        fileStream.Close();
                    }
                }
                downloaded = true;
                return true; //finished
            }
            return false; //something failed, the container doesn't exist
        }

        //Build the trie
        //Parameters: none
        //Returns: returns a query of "ekai" to enure the build worked
        [WebMethod]
        public List<string> BuildTrie()
        {
            //setup the file paths for the build
            string titlesTarget = Path.GetTempPath() + "wikipedia-titles";
            string countsTarget = Path.GetTempPath() + "pageview-counts";

            //build the Trie
            TitlesTrie = new TrieDualNode(200);
            StreamReader titlesSR = new StreamReader(titlesTarget);
            Regex reg = new Regex("^[a-zA-Z_]+$"); //regex to make sure we only grab titles we want
            while (titlesSR.EndOfStream == false)
            {
                string line = titlesSR.ReadLine();

                //if we have a match insert into the Trie
                if (reg.IsMatch(line))
                {
                    TitlesTrie.insert(line);
                }
            }

            //add pageview counts
            StreamReader countsSR = new StreamReader(countsTarget);
            while (countsSR.EndOfStream == false)
            {
                //split out the title and the # of pageviews
                string[] line = countsSR.ReadLine().Split(' ');
                TitlesTrie.addPagecount(line[0], Convert.ToInt32(line[1]));
            }

            //show that it finished and worked
            return TitlesTrie.query("ekai");
        }

        //returns the remaining system's remaining memory
        [WebMethod]
        public string GetWebRoleMemRemaining()
        {
            return memProcessWebRole.NextValue().ToString() + "MB";
        }

        ///////////////
        ///PA3 Stuff///
        ///////////////

        [WebMethod]
        public List<string> Tests()
        {
            Uri uri = new Uri("http://www.sports.cnn.com/football/games/something.html?game=17&time=105");
            List<string> ls = new List<string>();
            ls.Add(uri.Host);
            ls.Add(uri.HostNameType.ToString());
            ls.Add(uri.IsFile.ToString());
            ls.Add(uri.AbsolutePath);
            ls.Add((uri.AbsolutePath.Split('.')[1] == "html").ToString());
            ls.Add((uri.AbsoluteUri));
            ls.Add(uri.Host.Replace("www.", ""));

            ls.Add(new Uri("/bob/dole/index.html").Host);

            return ls;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Start(string args)
        {
            string result = "";
            try
            {
                args = Regex.Replace(args, @"\s+", " ");
                args = args.ToLower();

                storageManager.InsertCommand("start", args);
                result = "worked";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Stop()
        {
            string result = "";
            try
            {
                storageManager.InsertCommand("stop");
                result = "worked";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Clear()
        {
            string result = "";
            try
            {
                storageManager.ResetAll();
                Thread.Sleep(10000);
                result = "worked";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMemAvaliable()
        {
            string result = "";
            try
            {
                result = storageManager.GetDiagnostic("memoryavaliable") + "MB";
            }
            catch(Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCpuUsed()
        {
            string result = "";
            try
            {
                result = storageManager.GetDiagnostic("cpu") + "%";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMemUsed()
        {
            string result = "";
            try
            {
                result = storageManager.GetDiagnostic("memoryused") + "%";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetLast10Crawled()
        {
            string result = "";
            try
            {
                result = js.Serialize(storageManager.GetLast10());
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetQueueLength()
        {
            int total = 0;
            for (int i = 0; i < NUMBER_CRAWLER_THREADS; i++)
            {
                try
                {
                    total += int.Parse(storageManager.GetDiagnostic("queuelength" + i));
                }
                catch (Exception e)
                {
                    return "0";
                }
            }
            return total.ToString();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetTableLength()
        {
            int total = 0;
            for (int i = 0; i < NUMBER_CRAWLER_THREADS; i++)
            {
                try
                {
                    total += int.Parse(storageManager.GetDiagnostic("tablelength" + i));
                }
                catch (Exception e)
                {
                    return "0";
                }
            }
            return total.ToString();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCrawlerStatus()
        {
            string result = "";
            try
            {
                result = storageManager.GetCrawlerStatus();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCrawlerTargets()
        {
            string result = "";
            try
            {
                result = storageManager.GetCommandStatus().Url;
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }
        

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Search(string searchphrase)
        {
            string result = "";
            try
            {
                List<URLTableRow> listResults = new List<URLTableRow>();

                searchphrase = Regex.Replace(searchphrase, @"\s+", " ");
                foreach(string keyword in searchphrase.ToLower().Trim().Split(' '))
                {
                    //if in cache get from there
                    if (cache.ContainsKey(keyword))
                    {
                        listResults.AddRange(cache[keyword]);
                    }
                    else
                    {
                        List<URLTableRow> queryResults = storageManager.GetUrl(keyword);
                        listResults.AddRange(queryResults);
                        cache.Add(keyword, queryResults);
                        cacheQueue.Enqueue(keyword);
                        if (cacheQueue.Count > 100)
                        {
                            string wordToDelete = cacheQueue.Dequeue();
                            cache.Remove(wordToDelete);
                        }
                    }
                }

                //order results by top hits, then by Date, only take top 20
                var sortedResults = listResults.GroupBy(x => x.URL)
                    .Select(x => new Tuple<List<URLTableRow>, int, DateTime>(x.ToList(), x.ToList().Count, x.ToList()[0].Date))
                    .OrderByDescending(x => x.Item2).ThenByDescending(x => x.Item3)
                    .Take(20)
                    .Select(x => x.Item1)
                    .Select(x => x[0]);

                //Lookup table for URLs (might need new structures)
                result = js.Serialize(sortedResults);
                numSearches++;
            }
            catch (Exception e)
            {
                result = js.Serialize(new string[] { e.Message });
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetTrieSize()
        {
            string result = "";
            try
            {
                result = TitlesTrie.GetTotalTitles().ToString();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetLastTitle()
        {
            string result = "";
            try
            {
                result = TitlesTrie.GetLastTitle();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string IsDownloaded()
        {
            string result = "";
            try
            {
                result = downloaded.ToString();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string IsTrieBuilt()
        {
            string result = "";
            try
            {
                result = (TitlesTrie != null).ToString();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCacheSize()
        {
            string result = "";
            try
            {
                result = cacheQueue.Count.ToString();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ClearCache()
        {
            string result = "";
            try
            {
                cache = new Dictionary<string, List<URLTableRow>>();
                cacheQueue = new Queue<string>();
                numSearches = 0;
                result = "worked";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetNumSearches()
        {
            string result = "";
            try
            {
                result = numSearches.ToString();
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }
    }
}
