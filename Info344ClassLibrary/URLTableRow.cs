﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class URLTableRow : TableEntity
    {
        public string URL { get; set; }
        public string Word { get; set; }
        public DateTime Date { get; set; }
        public string FullTitle { get; set; }

        public URLTableRow(string url, string word, DateTime date, string title)
        {
            this.PartitionKey = word;
            this.RowKey = Base64Encode(url.ToLower());

            this.URL = url.ToLower();
            this.Word = word;
            this.Date = date;
            this.FullTitle = title;
        }

        public URLTableRow() { }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        private string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public string GetURL()
        {
            return Base64Decode(PartitionKey);
        }
    }
}
