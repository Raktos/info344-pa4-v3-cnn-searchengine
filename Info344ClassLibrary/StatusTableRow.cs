﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class StatusTableRow : TableEntity
    {
        public string Status { get; set; }
        public StatusTableRow(string status)
        {
            this.PartitionKey = "status";
            this.RowKey = "current";

            this.Status = status;
        }

        public StatusTableRow() { }
    }
}
