﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class ErrorTableRow : TableEntity
    {
        public string ErrorMessage { get; set; }

        public ErrorTableRow(string errorMessage)
        {
            this.PartitionKey = "error";
            this.RowKey = Guid.NewGuid().ToString();

            this.ErrorMessage = errorMessage;
        }

        public ErrorTableRow() { }
    }
}
