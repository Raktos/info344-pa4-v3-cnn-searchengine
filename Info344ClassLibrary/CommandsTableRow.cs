﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class CommandsTableRow : TableEntity
    {
        public string Command { get; set; }
        public string Url { get; set; }

        public CommandsTableRow(string command, string url = "")
        {
            this.PartitionKey = "command";
            this.RowKey = "current";

            this.Command = command;
            this.Url = url;
        }

        public CommandsTableRow() { }
    }
}
