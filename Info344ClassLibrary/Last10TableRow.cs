﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class Last10TableRow : TableEntity
    {
        public string Url { get; set; }
        public Last10TableRow(string url, int i)
        {
            this.PartitionKey = "last10";
            this.RowKey = i.ToString();

            this.Url = url;
        }

        public Last10TableRow() { }
    }
}
