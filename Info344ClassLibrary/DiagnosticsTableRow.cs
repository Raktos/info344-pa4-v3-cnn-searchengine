﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class DiagnosticsTableRow : TableEntity
    {
        public string Category { get; set; }
        public string Value { get; set; }

        public DiagnosticsTableRow(string category, string value)
        {
            this.PartitionKey = "diagnostics";
            this.RowKey = category;

            this.Category = category;
            this.Value = value;
        }

        public DiagnosticsTableRow() { }
    }
}
