﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class Crawler
    {
        private string[][] DisallowSets;
        private Uri[] BaseUrls;
        public List<string> LastResults { get; set; }
        public string LastTitle { get; set; }
        public string LastUrl { get; set; }
        public DateTime LastDate { get; set; }
        public Crawler(string[][] disallows, Uri[] baseUrls)
        {
            this.DisallowSets = disallows;
            this.BaseUrls = baseUrls;
        }

        public void crawl(string url)
        {
            Uri parsedUrl = new Uri(url);
            HtmlWeb webGet = new HtmlWeb();
            try
            {
                LastUrl = url;
                HtmlDocument htmlDoc = webGet.Load("http://" + parsedUrl.Host + parsedUrl.AbsolutePath);
                LastResults = new List<string>();

                var links = htmlDoc.DocumentNode.SelectNodes("//a[@href]");

                foreach (var link in links)
                {
                    string href = link.Attributes["href"].Value;
                    if (href == "#")
                    {
                        href = "/";
                    }
                    
                    if (href[0] == '/')
                    {
                        href = "http://" + parsedUrl.Host + href;
                    }
                    try
                    {
                        Uri newUrl = new Uri(href);
                        bool fileFormatPass = false;
                        if (!newUrl.AbsolutePath.Contains("."))
                        {
                            fileFormatPass = true;
                        }
                        else if (newUrl.AbsolutePath.Substring(newUrl.AbsolutePath.Length - 4) == ".htm")
                        {
                            fileFormatPass = true;
                        }
                        else if (newUrl.AbsolutePath.Substring(newUrl.AbsolutePath.Length - 5) == ".html")
                        {
                            fileFormatPass = true;
                        }

                        for (int i = 0; i < BaseUrls.Length; i++)
                        {
                            if (newUrl.Host.Contains(BaseUrls[i].Host) && fileFormatPass)
                            {
                                bool disallowed = false;
                                foreach (string disallow in DisallowSets[i])
                                {
                                    if (newUrl.AbsolutePath.Length > disallow.Length)
                                    {
                                        if (newUrl.AbsolutePath.Substring(0, disallow.Length - 1) == disallow ||
                                        newUrl.AbsolutePath.Substring(newUrl.AbsolutePath.Length - disallow.Length) == disallow)
                                        {
                                            disallowed = true;
                                            break;
                                        }
                                    }
                                }

                                if (!disallowed && !LastResults.Contains(href))
                                {
                                    LastResults.Add(href);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }

                var titleNode = htmlDoc.DocumentNode.SelectSingleNode("//title");
                LastTitle = titleNode.InnerText;

                var metas = htmlDoc.DocumentNode.SelectNodes("//meta");
                string date = "";
                foreach (var node in metas)
                {
                    string thisNodeType = node.GetAttributeValue("name", "");
                    if (thisNodeType == "lastmod")
                    {
                        date = node.GetAttributeValue("content", "");
                        break;
                    }
                }
                if(date == "")
                {
                    LastDate = DateTime.Now;
                }
                else
                {
                    LastDate = DateTime.Parse(date);
                }
            }
            catch (Exception e)
            {
                throw new Exception("failed to crawl " + url + " | " + e.Message);
            }
        }
    }
}
