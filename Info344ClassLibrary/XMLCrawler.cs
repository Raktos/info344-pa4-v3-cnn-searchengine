﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Info344ClassLibrary
{
    public class XMLCrawler
    {
        private DateTime MinDate;
        private Uri BaseUrl;
        private List<string> Disallows;
        private Queue<string> InitialUrls;
        private Queue<string> sitemapQueue = new Queue<string>();
        public XMLCrawler(Uri baseUrl, DateTime minDate)
        {
            this.BaseUrl = baseUrl;
            this.MinDate = minDate;
            this.Disallows = new List<string>();
            this.InitialUrls = new Queue<string>();

            this.BuildSitemap();
        }

        private void BuildSitemap()
        {
            WebRequest robotTxtRequest = WebRequest.CreateHttp(BaseUrl.AbsoluteUri + "/robots.txt");
            WebResponse robotTxtResponse = robotTxtRequest.GetResponse();
            StreamReader robotReader = new StreamReader(robotTxtResponse.GetResponseStream());

            while (robotReader.EndOfStream == false)
            {
                string[] line = robotReader.ReadLine().Split(' ');

                if (line[0] == "Sitemap:")
                {
                    sitemapQueue.Enqueue(line[1]);
                }
                else if (line[0] == "Disallow:")
                {
                    Disallows.Add(line[1]);
                }
            }

            while (sitemapQueue.Count > 0)
            {
                WebRequest sitemapRequest = WebRequest.CreateHttp(sitemapQueue.Dequeue());
                WebResponse sitemapResponse = sitemapRequest.GetResponse();
                XmlReader sitemap = XmlReader.Create(sitemapResponse.GetResponseStream());

                while (sitemap.Read())
                {
                    if (sitemap.IsStartElement())
                    {
                        if (sitemap.Name == "sitemap")
                        {
                            sitemap.ReadToDescendant("loc");
                            string newTarget = sitemap.ReadElementContentAsString();
                            DateTime lastMod;
                            if (sitemap.Name == "lastmod")
                            {
                                lastMod = sitemap.ReadElementContentAsDateTime();
                            }
                            else
                            {
                                lastMod = DateTime.Now;
                            }


                            if (lastMod > MinDate)
                            {
                                sitemapQueue.Enqueue(newTarget);
                            }
                        }
                        else if (sitemap.Name == "url")
                        {
                            sitemap.ReadToDescendant("loc");
                            string url = sitemap.ReadElementContentAsString();

                            InitialUrls.Enqueue(url);
                        }
                    }
                }
            }
        }

        public string[] GetDisallows()
        {
            return Disallows.ToArray();
        }

        public Queue<string> GetUrls()
        {
            return InitialUrls;
        }
    }
}
