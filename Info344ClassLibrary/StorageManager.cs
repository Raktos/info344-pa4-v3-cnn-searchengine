﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    public class StorageManager
    {
        public CloudStorageAccount StorageAccount { get; set; }
        public CloudQueueClient QueueClient { get; set; }
        public CloudTableClient TableClient { get; set; }
        public CloudQueue UrlQueue { get; set; }
        public CloudTable CommandsTable { get; set; }
        public CloudTable DiagnosticsTable { get; set; }
        public CloudTable UrlTable { get; set; }
        public CloudTable StatusTable { get; set; }
        public CloudBlobClient BlobClient { get; set; }
        public CloudBlobContainer BlobContainer { get; set; }
        public CloudTable ErrorsTable { get; set; }
        public string[] StopWords { get; set; }
        CloudTable Last10Table { get; set; }

        public StorageManager(CloudStorageAccount account)
        {
            this.StorageAccount = account;
            this.QueueClient = StorageAccount.CreateCloudQueueClient();
            this.TableClient = StorageAccount.CreateCloudTableClient();
            this.UrlQueue = QueueClient.GetQueueReference("urlqueue");
            this.CommandsTable = TableClient.GetTableReference("CommandsTable");
            this.DiagnosticsTable = TableClient.GetTableReference("DiagnosticsTable");
            this.UrlTable = TableClient.GetTableReference("URLTable");
            this.StatusTable = TableClient.GetTableReference("StatusTable");
            this.BlobClient = StorageAccount.CreateCloudBlobClient();
            this.BlobContainer = BlobClient.GetContainerReference("wikipedia");
            this.ErrorsTable = TableClient.GetTableReference("ErrorsTable");
            this.Last10Table = TableClient.GetTableReference("Last10Table");

            UrlQueue.CreateIfNotExists();
            CommandsTable.CreateIfNotExists();
            DiagnosticsTable.CreateIfNotExists();
            UrlTable.CreateIfNotExists();
            StatusTable.CreateIfNotExists();
            ErrorsTable.CreateIfNotExists();
            Last10Table.CreateIfNotExists();

            this.StopWords = new string[] {"a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"};
        }

        public void InsertCommand(string command, string args = "")
        {
            try
            {
                TableOperation insertCommand = TableOperation.InsertOrReplace(new CommandsTableRow(command, args));
                CommandsTable.Execute(insertCommand);
            }
            catch (Exception e)
            {
                InsertError("Command insert failed | " + command + " " + args + " | " + e.Message);
            }
        }

        public void InsertDiagnostic(string category, string value)
        {
            try
            {
                TableOperation insertDiagnostic = TableOperation.InsertOrReplace(new DiagnosticsTableRow(category, value));
                DiagnosticsTable.Execute(insertDiagnostic);
            }
            catch (Exception e)
            {
                InsertError("Diagnostic insert failed | " + category + " " + value + " | " + e.Message);
            }
        }

        public void InsertToQueue(string url, int threadNum)
        {
            try
            {
                UrlQueue.AddMessage(new CloudQueueMessage(url));
                IncrementQueueLength(threadNum);
            }
            catch (Exception e)
            {
                InsertError("Queue insert failed | thread" + threadNum + " " + url + " | " + e.Message);
            }
        }

        public void DeleteFromQueue(CloudQueueMessage queueItem, int threadNum)
        {
            try
            {
                UrlQueue.DeleteMessage(queueItem);
                DecrementQueueLength(threadNum);
            }
            catch (Exception e)
            {
                InsertError("Queue delete failed | thread" + threadNum + " " + queueItem.AsString + " | " + e.Message);
            }
        }

        private void DecrementQueueLength(int threadNum)
        {
            try
            {
                TableOperation insertQueueLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("queuelength" + threadNum, (GetQueueLength(threadNum) - 1).ToString()));
                DiagnosticsTable.Execute(insertQueueLength);
            }
            catch (Exception e)
            {
                InsertError("Queue decrement failed | thread" + threadNum + " | " + e.Message);
            }
        }

        private void IncrementQueueLength(int threadNum)
        {
            try
            {
                TableOperation insertQueueLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("queuelength" + threadNum, (GetQueueLength(threadNum) + 1).ToString()));
                DiagnosticsTable.Execute(insertQueueLength);
            }
            catch (Exception e)
            {
                InsertError("Queue increment failed | thread" + threadNum + " | " + e.Message);
            }
        }

        public int GetQueueLength(int threadNum)
        {
            try
            {
                TableOperation getLength = TableOperation.Retrieve<DiagnosticsTableRow>("diagnostics", "queuelength" + threadNum);
                TableResult getLengthResult = DiagnosticsTable.Execute(getLength);
                if (getLengthResult.Result != null)
                {
                    return int.Parse(((DiagnosticsTableRow)getLengthResult.Result).Value);
                }
            }
            catch (Exception e)
            {
                InsertError("Queue length grab failed | thread" + threadNum + " | " + e.Message);
            }
            return 0;
        }

        public CommandsTableRow GetCommandStatus()
        {
            try
            {
                CommandsTable.CreateIfNotExists();
                TableOperation getCommand = TableOperation.Retrieve<CommandsTableRow>("command", "current");
                TableResult getCommandResult = CommandsTable.Execute(getCommand);
                if (getCommandResult.Result != null)
                {
                    return ((CommandsTableRow)getCommandResult.Result);
                }
            }
            catch (Exception e)
            {
                InsertError("Command grab failed | " + e.Message);
            }
            return new CommandsTableRow("no commands");
        }

        public string GetCrawlerStatus()
        {
            try
            {
                CommandsTable.CreateIfNotExists();
                TableOperation getStatus = TableOperation.Retrieve<StatusTableRow>("status", "current");
                TableResult getStatusResult = StatusTable.Execute(getStatus);
                if (getStatusResult.Result != null)
                {
                    return ((StatusTableRow)getStatusResult.Result).Status;
                }
            }
            catch (Exception e)
            {
                InsertError("Status grab failed | " + e.Message);
            }
            return "loading...";
        }

        public void SetStatus(string status)
        {
            try
            {
                StatusTable.CreateIfNotExists();
                TableOperation updateStatus = TableOperation.InsertOrReplace(new StatusTableRow(status));
                StatusTable.Execute(updateStatus);
            }
            catch (Exception e)
            {
                InsertError("Status set failed | " + status + " | " + e.Message);
            }
        }

        public CloudQueueMessage GetQueueItem()
        {
            try
            {
                return UrlQueue.GetMessage(TimeSpan.FromMinutes(5));
            }
            catch (Exception e)
            {
                InsertError("Queue grab failed | " + e.Message);
            }
            return new CloudQueueMessage("failed");
        }

        public void InsertUrl(string url, string title, DateTime date, int threadNum)
        {
            Regex reg = new Regex("^[a-z]+$");
            foreach (string word in title.ToLower().Replace('-', ' ').Split(' '))
            {
                if (word.Length > 1 && !StopWords.Contains(word) && reg.IsMatch(word))
                {
                    try
                    {
                        URLTableRow newUrlRow = new URLTableRow(url, word, date, title);
                        TableOperation insertUrl = TableOperation.Insert(newUrlRow);
                        UrlTable.Execute(insertUrl);
                    }
                    catch(Exception e)
                    {
                        InsertError("URL insert failed | thread" + threadNum + " " + url + " | " + e.Message);
                    }
                }
            }

            IncrementTableLength(threadNum);
        }

        private void DecrementTableLength(int threadNum)
        {
            try
            {
                TableOperation insertTableLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("tablelength" + threadNum, (GetTableLength(threadNum) - 1).ToString()));
                DiagnosticsTable.Execute(insertTableLength);
            }
            catch (Exception e)
            {
                InsertError("Table decrement failed | thread" + threadNum + " | " + e.Message);
            }
        }

        private void IncrementTableLength(int threadNum)
        {
            try
            {
                TableOperation insertTableLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("tablelength" + threadNum, (GetTableLength(threadNum) + 1).ToString()));
                DiagnosticsTable.Execute(insertTableLength);
            }
            catch (Exception e)
            {
                InsertError("Table increment failed | thread" + threadNum + " | " + e.Message);
            }
        }

        public int GetTableLength(int threadNum)
        {
            try
            {
                TableOperation getLength = TableOperation.Retrieve<DiagnosticsTableRow>("diagnostics", "tablelength" + threadNum);
                TableResult getLengthResult = DiagnosticsTable.Execute(getLength);
                if (getLengthResult.Result != null)
                {
                    return int.Parse(((DiagnosticsTableRow)getLengthResult.Result).Value);
                }
            }
            catch (Exception e)
            {
                InsertError("Table length grab failed | thread" + threadNum + " | " + e.Message);
            }
            return 0;
        }

        public void InsertError(string errorMessage)
        {
            try
            {
                ErrorTableRow newError = new ErrorTableRow(errorMessage);
                TableOperation insertError = TableOperation.Insert(newError);
                ErrorsTable.Execute(insertError);
            }
            catch (Exception e)
            {
                //shit. The error handling failed. Now we're really in trouble.
            }
        }

        public List<string> GetUrls(string keyword)
        {
            List<string> results = new List<string>();
            try
            {
                
            }
            catch (Exception e)
            {
                InsertError("Failed to get keyword " + keyword + " | " + e.Message);
            }
            return results;
        }

        public void ResetAll()
        {
            try
            {
                UrlQueue.Clear();
                CommandsTable.DeleteIfExists();
                DiagnosticsTable.DeleteIfExists();
                ErrorsTable.DeleteIfExists();
                UrlTable.DeleteIfExists();
                Last10Table.DeleteIfExists();
            }
            catch (Exception e)
            {
                InsertError("Failed to reset | " + e.Message);
            }
        }

        public string GetDiagnostic(string category)
        {
            try
            {
                TableOperation getDiag = TableOperation.Retrieve<DiagnosticsTableRow>("diagnostics", category);
                TableResult getMemResult = DiagnosticsTable.Execute(getDiag);
                if (getMemResult.Result != null)
                {
                    return ((DiagnosticsTableRow)getMemResult.Result).Value;
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "loading...";
        }

        public void Last10Insert(string url, int i)
        {
            try
            {
                Last10TableRow newLast10 = new Last10TableRow(url, i);
                TableOperation insertUrl = TableOperation.InsertOrReplace(newLast10);
                Last10Table.Execute(insertUrl);
            }
            catch (Exception e)
            {
                InsertError("Failed to insert Last10URL " + url + " | " + e.Message);
            }
        }

        public List<Last10TableRow> GetLast10()
        {
            List<Last10TableRow> last10 = new List<Last10TableRow>();
            try
            {
                TableQuery<Last10TableRow> query = new TableQuery<Last10TableRow>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "last10"));

                last10 = Last10Table.ExecuteQuery(query).ToList();
            }
            catch (Exception e)
            {
                InsertError("Failed to get last 10 | " + e.Message);
            }
            return last10;
        }

        public List<URLTableRow> GetUrl(string keyword)
        {
            try
            {
                TableQuery<URLTableRow> query = new TableQuery<URLTableRow>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, keyword.ToLower().Trim()));

                return UrlTable.ExecuteQuery(query).ToList();
            }
            catch (Exception e)
            {
                InsertError("Failed to grab URL | keyword:" + keyword + " | " + e.Message);
            }
            return new List<URLTableRow>();
        }
    }
}
