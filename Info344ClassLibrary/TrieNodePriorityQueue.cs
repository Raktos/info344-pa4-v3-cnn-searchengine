﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    /// <summary>
    /// Priority Queue for holding the order subtrees should be searched
    /// Highest priority is levenshtein match (we want the closest matches most), second is viewcount (we want more popular results if edit distance is the same)
    /// </summary>
    class TrieNodePriorityQueue
    {
        private Node[] heap; //the actual structure, an array
        private int size; //current size of the heap

        //Node class, the data in the heap
        private class Node
        {
            public char key; //the character
            public double popularity; //pageview count
            public int[] levenshteinRow; //the levRow

            //constructs a new Node
            public Node(char c, int totalCount, int children, int[] iArr)
            {
                this.key = c;
                //this.popularity = totalCount / (double)children;
                this.popularity = 0;
                this.levenshteinRow = iArr;
            }

        }

        //constructs a new priority queue
        //Parameters: int size| initial array size
        public TrieNodePriorityQueue(int size = 25)
        {
            heap = new Node[25 + 1];
            this.size = 0;
        }

        //insert an item into the queue
        //Parameters: char key, int count, int[] levenscteinRow
        public void insert(char key, int count, int children, int[] levenschteinRow)
        {
            Node n = new Node(key, count, children, levenschteinRow);

            //resize if the insert will break the array
            if (size == heap.Length - 1)
            {
                resize();
            }
            size++;

            //find where the new insert should go and put it there
            int hole = percolateUp(size, n);
            heap[hole] = n;
        }

        //percolate an insert up from the last possible position until it finds a valid slot
        //Parameters: int hole| initial hole, Node n| node to insert
        //Returns: returns the int of the true hole, where the insert should go
        private int percolateUp(int hole, Node n)
        {
            //until we reach the top position, keep checking
            while (hole > 1)
            {
                //prepare for comparisions
                int editDistance = n.levenshteinRow.Min();
                int parentEditDistance = heap[hole / 2].levenshteinRow.Min();

                //if this is a higher priority node than the parent, move it up
                if (editDistance < parentEditDistance || (editDistance == parentEditDistance && n.popularity > heap[hole / 2].popularity))
                {
                    heap[hole] = heap[hole / 2];
                    hole = hole / 2;
                }
                else
                {
                    //we found the position we want, break the loop
                    break;
                }
            }
            return hole;
        }

        //removes the top item from the queue
        public void delete()
        {
            //do nothing if the queue is empty
            //prolly should throw an exception but I was lazy
            if (size <= 0)
            {
                throw new Exception("Queue empty");
            }

            //remove the top item and fix the queue
            int hole = percolateDown(1, heap[size]);
            heap[hole] = heap[size];
            size--;
        }

        //percolates down from the top
        //Parameters: int hole| initial hole, Node n| node to move down
        //Returns: returns the int of the true hole, where the last item should be moved to
        private int percolateDown(int hole, Node n)
        {
            //loops until we reach size, we can't go past the end of the array
            while (2 * hole <= size)
            {
                //find indexs of children nodes
                int left = 2 * hole;
                int right = left + 1;
                int target;

                //get data on child nodes
                int editDistance = n.levenshteinRow.Min();
                int leftChildEditDistance = heap[left].levenshteinRow.Min();
                int rightChildEditDistance = heap[right] == null ? 9999 : heap[right].levenshteinRow.Min();
                int targetEditDistance;

                //left is higher priority than right, go that way
                if (right > size || leftChildEditDistance < rightChildEditDistance || (leftChildEditDistance == rightChildEditDistance && heap[left].popularity > heap[right].popularity))
                {
                    target = left;
                    targetEditDistance = leftChildEditDistance;
                }
                else //right is higher priority
                {
                    target = right;
                    targetEditDistance = rightChildEditDistance;
                }

                //our target Node is higher priority than ours, move down and check
                if (targetEditDistance < editDistance || (targetEditDistance == editDistance && heap[target].popularity > n.popularity))
                {
                    heap[hole] = heap[target];
                    hole = target;
                }
                else
                {
                    //we found the position we want, break the loop
                    break;
                }
            }
            return hole;
        }

        //peeks the key of the top item
        public char peekKey()
        {
            if (size <= 0)
            {
                return '\0';
            }
            return heap[1].key;
        }

        //peeks the viewcount of the top item
        public double peekCount()
        {
            if (size <= 0)
            {
                return -1;
            }
            return heap[1].popularity;
        }

        //peeks the levRow of the top item
        public int[] peekLevenschtein()
        {
            if (size <= 0)
            {
                return null;
            }
            return heap[1].levenshteinRow;
        }

        //resizes the array
        private void resize()
        {
            Node[] newHeap = new Node[heap.Length * 2];

            for (int i = 1; i < heap.Length; i++)
            {
                newHeap[i] = heap[i];
            }

            heap = newHeap;
        }

        //returns if the queue is empty or not
        public bool isEmpty()
        {
            return size <= 0;
        }
    }
}
