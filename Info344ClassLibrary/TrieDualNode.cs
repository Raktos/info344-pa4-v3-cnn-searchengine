﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info344ClassLibrary
{
    /// <summary>
    /// A hybrid Trie, leaves are linked lists
    /// </summary>
    public class TrieDualNode
    {
        private TrieNode root; //root
        private int linkedListSize; //size of linked lists before conversion to a Trie structure
        private int MAX_EDIT_DISTANCE; //maximum allowed edit distance for spelling correction
        private int MAX_RESULTS;
        private int TotalTitles;
        private string LastInsert;

        //TrieNode, internal Node structure for TrieNodes
        private class TrieNode
        {
            public char letter; //letter of the Node
            public ListNode listChildren; //pointer to the head of the linked list for LinkedList children
            public Dictionary<char, TrieNode> dictChildren; //the dictionary containing TrieNode childred
            public int numChildren; //number of children in the linked list
            public bool isPhrase; //marks if this node is the end of a phrase
            public int viewCount; //views of this Node, if it is a phrase (can still be 0, even if a phrase)
            public int subtreeViewCount; //views of the entire subtree gorwing from this Node

            //consturct a new Node of a given character
            //Parameters: takes a character (defaults to null character)
            public TrieNode(char input = '\0')
            {
                this.letter = input;
                this.listChildren = null;
                this.dictChildren = null;
                this.numChildren = 0;
                this.viewCount = 0;
                this.subtreeViewCount = 0;
                this.isPhrase = false;
            }
        }

        //ListNode, internal node structure for linked lists
        private class ListNode
        {
            public string phrase; //linked list Nodes contain a phrase
            public ListNode next; //Next node in the list
            public int viewCount; //views of this Node's phrase

            //constructs a ListNode of given input
            //Parameters: takes a string input
            public ListNode(string input)
            {
                this.phrase = input;
                this.next = null;
                this.viewCount = 0;
            }
        }

        //Constructs a new Hybrid Trie
        //Parameters: takes an int for the linked list length, and an int for the max edit distance
        public TrieDualNode(int listSize = 50, int editDistance = 1, int maxResults = 100)
        {
            this.root = new TrieNode();
            this.linkedListSize = listSize;
            this.MAX_EDIT_DISTANCE = editDistance;
            this.MAX_RESULTS = maxResults;
            this.TotalTitles = 0;
            this.LastInsert = "";
        }

        //Public method to insert a new phrase into the Trie
        //ParametersL takes a string input
        public void insert(string input)
        {
            insert(input.ToLower(), root);
            LastInsert = input;
            TotalTitles++;
        }

        //Private helper method to recursively insert a phrase into the Trie
        //Parameters: takes a string of input, and a TrieNode to insert into
        private void insert(string input, TrieNode n)
        {
            //we reached a Node without a full linked list, we want to insert into the linked list
            if (n.numChildren < linkedListSize)
            {
                //Make the new node, put it at the head of the linked list
                ListNode newNode = new ListNode(input);
                newNode.next = n.listChildren;
                n.listChildren = newNode;
                n.numChildren++;
            }
            else if (n.dictChildren == null) //Linked list is full, but there isn't a ditionary set of children, this node needs to be converted
            {
                //initialize the dictionary
                n.dictChildren = new Dictionary<char, TrieNode>(4);

                //until we run out of items in the linked list insert them, they will end up being inserted into a Trie structure
                while (n.listChildren != null)
                {
                    insert(n.listChildren.phrase, n);
                    n.numChildren--;
                    n.listChildren = n.listChildren.next;
                }
                //now that we've converted the Node we need to insert what we originally planned to insert
                insert(input, n);
            }
            else //We're not down far enough yet, keep going
            {
                n.numChildren++;

                //grab the first character
                char firstChar = input[0];

                //if there is no child for the first char make it
                if (!n.dictChildren.ContainsKey(firstChar))
                {
                    n.dictChildren[firstChar] = new TrieNode(firstChar);
                }

                //if end of phrase
                if (input.Length <= 1)
                {
                    n.dictChildren[firstChar].isPhrase = true;
                }
                else //we reallt need to go firther down, go down through first char, insert the input minus the character we just passed
                {
                    insert(input.Substring(1), n.dictChildren[firstChar]);
                }
            }
        }

        //query the Trie
        //Parameters: takes a string input
        //Returns: returns a list of strings containing the result set ordered by page views descending
        public List<string> query(string input)
        {
            //initialize the first row of a levenshtein matrix
            int[] currentRow = new int[input.Length + 1];
            for (int i = 0; i < currentRow.Length; i++)
            {
                currentRow[i] = i;
            }

            //recursively query the input
            List<KeyValuePair<string, int>> results = query(input, "", root, currentRow);

            //reoder the list by pageviews
            results = results.OrderByDescending(x => x.Value).ToList();

            //convert to a list of just strings
            List<string> finalResults = new List<string>();
            foreach (var item in results)
            {
                finalResults.Add(item.Key);
            }

            return finalResults;
        }

        //Private helper function to recursively query the Trie
        //Parameteres: string input| input, string currPhrase| current phrase, used for reconstruction when results are found, TrieNode n| current Node, int[] prevRow| previous levenshtein row
        //Returns: returns a list of KeyValuePairs, key the string of the match, value the pageview count of the item
        private List<KeyValuePair<string, int>> query(string input, string currPhrase, TrieNode n, int[] prevRow)
        {
            //initialize result list
            List<KeyValuePair<string, int>> results = new List<KeyValuePair<string, int>>();

            //initialize target (whole original input)
            //for use in levenshtein calculations
            string target = currPhrase + input;

            //We have a good phrase, and our input is out. This is a suggestion we want
            if (input.Length <= 0 && n.isPhrase)
            {
                results.Add(new KeyValuePair<string, int>(currPhrase, n.viewCount));
            }

            //reached a linked list (though it may be empty)
            if (n.dictChildren == null)
            {
                ListNode t = n.listChildren;

                //loop through the linked list and check for matches
                while (t != null)
                {
                    //If we have some input left, and out input is shorter than the Node's phrase...
                    if (input.Length <= t.phrase.Length && input.Length > 0)
                    {
                        //prepare the next rows of the levenshtein until we have a full matrix enting at the input's length
                        int[] currRow = prevRow;
                        foreach (char c in t.phrase.Substring(0, input.Length))
                        {
                            currRow = levenschteinNextRow(currRow, target, c);
                        }

                        //if we have a match within maximum edit distance add the result
                        if (currRow[target.Length] <= MAX_EDIT_DISTANCE)
                        {
                            results.Add(new KeyValuePair<string, int>(currPhrase + t.phrase, t.viewCount));
                        }
                    }
                    else if (input.Length - t.phrase.Length <= MAX_EDIT_DISTANCE)
                    {
                        results.Add(new KeyValuePair<string, int>(currPhrase + t.phrase, t.viewCount));
                    }
                    t = t.next;
                }
            }
            else //this is a TrieNode, we need to go further down
            {
                //prepare a queue, to get results in most desirable order
                TrieNodePriorityQueue queue = new TrieNodePriorityQueue(27);

                //No more input, these are the results we want
                if (input.Length <= 0)
                {
                    //prepare a queue of all possible subtrees
                    foreach (var item in n.dictChildren)
                    {
                        int[] dumArr = { 0 };
                        queue.insert(item.Key, item.Value.subtreeViewCount, item.Value.numChildren, dumArr);
                    }

                    //query on the items in the queue
                    while (!queue.isEmpty())
                    {
                        results.AddRange(query(input, currPhrase + queue.peekKey(), n.dictChildren[queue.peekKey()], prevRow));

                        //if we have too many results break out of the whole thing
                        if (results.Count > MAX_RESULTS)
                        {
                            break;
                        }
                        queue.delete();
                    }
                }
                else //we still have input left, move further down the trie if we can
                {
                    //prepare a queue of all possible subtrees
                    foreach (var item in n.dictChildren)
                    {
                        int[] currRow = levenschteinNextRow(prevRow, target, item.Key);

                        //only bother adding the item to the queue if it passes our minimum edit distance
                        if (currRow[currPhrase.Length + 1] <= MAX_EDIT_DISTANCE)
                        {
                            queue.insert(item.Key, item.Value.subtreeViewCount, item.Value.numChildren, currRow);
                        }

                    }

                    //go through the queue
                    while (!queue.isEmpty())
                    {
                        results.AddRange(query(input.Substring(1), currPhrase + queue.peekKey(), n.dictChildren[queue.peekKey()], queue.peekLevenschtein()));

                        //if we have too many results break out of the whole thing
                        if (results.Count > MAX_RESULTS)
                        {
                            break;
                        }
                        queue.delete();
                    }
                }
            }
            return results;
        }

        //compute the next row of a levenshtein based on the input
        //Parameters: int[] prevRow, string target| full initial input, char s| next character if potential match
        //Returns: returns an integer array of the next row in a levenshtein
        private int[] levenschteinNextRow(int[] prevRow, string target, char s)
        {
            int[] currRow = new int[prevRow.Length];

            currRow[0] = prevRow[0] + 1;

            //determine edit distance for each character of the target vs the current character
            for (int i = 0; i < target.Length; i++)
            {
                int cost = (target[i] == s) ? 0 : 1;
                currRow[i + 1] = Math.Min(Math.Min(currRow[i] + 1, prevRow[i] + 1), prevRow[i] + cost);
            }

            return currRow;
        }

        //add a pagecount to an input, if it exists
        //Parameters: string input, int viewcount
        public void addPagecount(string input, int count)
        {
            addPagecount(input.ToLower(), count, root);
        }

        //private helper method to addPageCount
        //Parameters: string input, int count, TrieNode n
        //returns an int, count if insert sucessful, 0 if not
        private int addPagecount(string input, int count, TrieNode n)
        {
            //if we have no input left this is the node we want
            if (input.Length <= 0)
            {
                //if this Node is a phrase update counts
                if (n.isPhrase)
                {
                    n.viewCount = count;
                    n.subtreeViewCount += count;
                    return count;
                }
                return 0;
            }

            //the children are in a linked list
            if (n.dictChildren == null)
            {
                ListNode t = n.listChildren;

                //go through the linked list looking for out input
                while (t != null)
                {
                    //we found the right input
                    if (t.phrase == input)
                    {
                        t.viewCount = count;
                        n.subtreeViewCount += count;
                        return count;
                    }
                    t = t.next;
                }
                return 0;
            }

            //We're not far enough down, add the result of a further query down the tree
            if (n.dictChildren.ContainsKey(input[0]))
            {
                n.subtreeViewCount += addPagecount(input.Substring(1), count, n.dictChildren[input[0]]);
            }

            //cannot find input, return 0
            return 0;
        }

        public int GetTotalTitles()
        {
            return TotalTitles;
        }
        public string GetLastTitle()
        {
            return LastInsert;
        }
    }
}
