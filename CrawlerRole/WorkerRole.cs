using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Concurrent;
using System.Configuration;
using Info344ClassLibrary;
using System.Text.RegularExpressions;
using System.Text;

namespace CrawlerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private CancellationTokenSource crawlerCancellationTokenSource = new CancellationTokenSource();
        private readonly CancellationTokenSource generalCancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private static readonly int NUMBER_CRAWLER_THREADS = 5;

        //load diagnostic counters
        private static PerformanceCounter memAvaliable = new PerformanceCounter("Memory", "Available MBytes");
        private static PerformanceCounter percentMemUsed = new PerformanceCounter("Memory", "% Committed Bytes In Use");
        private static PerformanceCounter procUsed = new PerformanceCounter("Processor", "% Processor Time", "_Total");

        //load cloudStorage connection and connections to tables and queues
        //private static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
        //private static CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
        //private static CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
        //private static CloudQueue urlQueue = queueClient.GetQueueReference("urlqueue");
        //private static CloudTable commandsTable = tableClient.GetTableReference("CommandsTable");
        //private static CloudTable diagnosticsTable = tableClient.GetTableReference("DiagnosticsTable");
        //private static CloudTable urlTable = tableClient.GetTableReference("URLTable");
        //private static CloudTable statusTable = tableClient.GetTableReference("StatusTable");

        static StorageManager storageManager;

        //private static Uri baseUrl;
        //private static string[] disallows;
        private static Uri[] baseUrls;
        private static string[][] disallowSets;

        private static ConcurrentQueue<string> last10UrlConcurrentQueue;
        private static ConcurrentDictionary<string, bool> processedUrls;

        public override void Run()
        {
            Trace.TraceInformation("CrawlerManager is starting");

            storageManager = new StorageManager(CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]));

            //reset start command
            storageManager.InsertCommand("no commands");

            //bool for tracking start
            bool shouldStart = false;

            //start diagnostic and cancelation managers
            RunDiagnosticsManager(generalCancellationTokenSource.Token);
            RunCancelManager(generalCancellationTokenSource.Token);

            while (true)
            {
                Trace.TraceInformation("CrawlerManager is idle");
                //TableOperation updateStatus = TableOperation.InsertOrReplace(new StatusTableRow("idle"));
                //statusTable.Execute(updateStatus);

                storageManager.SetStatus("idle");

                //check if we've recieved a start command
                CommandsTableRow curCommand = storageManager.GetCommandStatus();
                if (curCommand.Command == "start")
                {
                    //make sure all queues and tables exist


                    //get urls to crawl
                    string newUrlsRaw = curCommand.Url;
                    newUrlsRaw = Regex.Replace(newUrlsRaw, @"\s+", " ");
                    newUrlsRaw = newUrlsRaw.ToLower().Trim();
                    string[] newUrlsArr = newUrlsRaw.Split(' ');

                    baseUrls = new Uri[newUrlsArr.Length];

                    for (int i = 0; i < newUrlsArr.Length; i++)
                    {
                        if (!newUrlsArr[i].Contains("http://"))
                        {
                            newUrlsArr[i] = "http://" + newUrlsArr[i];
                        }
                        newUrlsArr[i] = newUrlsArr[i].Replace("www.", "");
                        baseUrls[i] = new Uri(newUrlsArr[i]);
                    }
                    shouldStart = true;
                }

                if (shouldStart)
                {
                    //set status to loading
                    Trace.TraceInformation("CrawlerManager is loading");
                    storageManager.SetStatus("loading");

                    //reset urls
                    processedUrls = new ConcurrentDictionary<string, bool>();

                    //reset cancellation token and event notifier
                    crawlerCancellationTokenSource.Dispose();
                    crawlerCancellationTokenSource = new CancellationTokenSource();
                    runCompleteEvent.Reset();

                    //load sitemap and robots.txt
                    XMLCrawler[] sitemaps = new XMLCrawler[baseUrls.Length];
                    disallowSets = new string[baseUrls.Length][];

                    last10UrlConcurrentQueue = new ConcurrentQueue<string>();

                    for (int i = 0; i < baseUrls.Length; i++)
                    {
                        sitemaps[i] = new XMLCrawler(baseUrls[i], DateTime.Parse("4-1-2015"));
                        disallowSets[i] = sitemaps[i].GetDisallows();
                        foreach (string url in sitemaps[i].GetUrls())
                        {
                            last10UrlConcurrentQueue.Enqueue(url);
                        }
                    }

                    sitemaps = null;

                    try
                    {
                        Trace.TraceInformation("CrawlerManager is populating initial queue");

                        var tasks = new List<Task>();

                        for (int i = 0; i < NUMBER_CRAWLER_THREADS; i++)
                        {
                            tasks.Add(PopulateQueue(i, crawlerCancellationTokenSource.Token));
                        }
                        Task.WaitAll(tasks.ToArray());
                    }
                    finally
                    {
                        Trace.TraceInformation("CrawlerManager populated initial queue");
                        this.runCompleteEvent.Set();
                    }

                    try
                    {
                        Trace.TraceInformation("CrawlerManager is running");
                        //updateStatus = TableOperation.InsertOrReplace(new StatusTableRow("running"));
                        //statusTable.Execute(updateStatus);

                        storageManager.SetStatus("running");

                        var tasks = new List<Task>();

                        for (int i = 0; i < NUMBER_CRAWLER_THREADS; i++)
                        {
                            tasks.Add(RunCrawlerAsync(this.crawlerCancellationTokenSource.Token, i));
                        }
                        tasks.Add(PostLast10(this.crawlerCancellationTokenSource.Token));
                        Task.WaitAll(tasks.ToArray());
                        //this.RunAsync(this.cancellationTokenSource.Token).Wait();
                    }
                    finally
                    {
                        Trace.TraceInformation("CrawlerManager is stopping");
                        //updateStatus = TableOperation.InsertOrReplace(new StatusTableRow("stopping"));
                        //statusTable.Execute(updateStatus);

                        storageManager.SetStatus("stopping");
                        shouldStart = false;
                        this.runCompleteEvent.Set();
                    }
                }
                Thread.Sleep(2000);
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 48;
            ServicePointManager.UseNagleAlgorithm = false;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("CrawlerManager has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("CrawlerManager is stopping");

            this.crawlerCancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("CrawlerManager has stopped");
        }

        private async Task RunCrawlerAsync(CancellationToken cancellationToken, int threadNum)
        {
            Trace.TraceInformation("Starting crawler thread {0}...", threadNum);
            Crawler crawler = new Crawler(disallowSets, baseUrls);
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Crawler thread {0} is running", threadNum);
                await Task.Delay(50);

                //CloudQueueMessage queueItem = urlQueue.GetMessage(TimeSpan.FromMinutes(5));
                CloudQueueMessage queueItem = storageManager.GetQueueItem();

                try
                {
                    crawler.crawl(queueItem.AsString);
                    List<string> newUrls = crawler.LastResults;

                    foreach (string url in newUrls)
                    {
                        if (!UrlAlreadyCrawled(url))
                        {
                            processedUrls.AddOrUpdate(url, true, (key, x) => x);
                            //urlQueue.AddMessage(new CloudQueueMessage(url));
                            //InsertQueueLength(threadNum, GetQueueLength(threadNum) + 1);
                            storageManager.InsertToQueue(url, threadNum);
                        }
                    }
                }
                catch (Exception e)
                {
                    storageManager.InsertError("Crawler failed to crawl " + queueItem.AsString + " | " + e.Message);
                    Trace.TraceInformation(e.Message);
                }

                try
                {
                    //still old version
                    //URLTableRow newUrlRow = new URLTableRow(queueItem.AsString, crawler.LastTitle);
                    //TableOperation insertUrl = TableOperation.Insert(newUrlRow);
                    //urlTable.Execute(insertUrl);
                    //InsertTableLength(threadNum, GetTableLength(threadNum) + 1);

                    storageManager.InsertUrl(crawler.LastUrl, crawler.LastTitle, crawler.LastDate, threadNum);

                    last10UrlConcurrentQueue.Enqueue(queueItem.AsString);

                    if (last10UrlConcurrentQueue.Count > 10)
                    {
                        string s;
                        last10UrlConcurrentQueue.TryDequeue(out s);
                    }

                    //InsertQueueLength(threadNum, GetQueueLength(threadNum) - 1);
                    //urlQueue.DeleteMessage(queueItem);
                    storageManager.DeleteFromQueue(queueItem, threadNum);
                }
                catch(Exception e)
                {
                    storageManager.InsertError("Failed to insert " + queueItem.AsString + " | " + e.Message);
                }
            }
        }

        private void InsertFinishedUrl()
        {

        }

        private async Task RunCancelManager(CancellationToken cancellationToken)
        {
            Trace.TraceInformation("CancelManager is starting");
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("CancelManager is running");
                await Task.Delay(2000);

                //check if we've recieved a stop command
                //CommandsTableRow curCommand = GetCommandStatus();
                CommandsTableRow curCommand = storageManager.GetCommandStatus();
                if (curCommand.Command == "stop")
                {
                    crawlerCancellationTokenSource.Cancel();
                    //TableOperation insertBlankCommand = TableOperation.InsertOrReplace(new CommandsTableRow("no commands"));
                    //commandsTable.Execute(insertBlankCommand);
                    storageManager.InsertCommand("no commands");
                }
            }
        }

        //private CommandsTableRow GetCommandStatus()
        //{
        //    TableOperation getCommand = TableOperation.Retrieve<CommandsTableRow>("command", "current");
        //    TableResult getCommandResult = commandsTable.Execute(getCommand);
        //    if (getCommandResult.Result != null)
        //    {
        //        return ((CommandsTableRow)getCommandResult.Result);
        //    }
        //    return new CommandsTableRow("no commands");
        //}

        private bool UrlAlreadyCrawled(string url)
        {
            return processedUrls.ContainsKey(url);
        }

        //private bool UrlAlreadyCrawledAzureTable(string url)
        //{
        //    TableOperation getUrl = TableOperation.Retrieve<CommandsTableRow>("crawledurls", Convert.ToBase64String(Encoding.UTF8.GetBytes(url)));
        //    TableResult getUrlResult = urlTable.Execute(getUrl);
        //    if (getUrlResult.Result == null)
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        private async Task RunDiagnosticsManager(CancellationToken cancellationToken)
        {
            Trace.TraceInformation("DiagnosticsManager is starting");
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("DiagnosticsManager is Running");
                await Task.Delay(10000);

                //get all stats and post them to table
                //TableOperation insertAvaliableMem = TableOperation.InsertOrReplace(new DiagnosticsTableRow("memoryavaliable", memAvaliable.NextValue().ToString()));
                //TableOperation insertUsedMem = TableOperation.InsertOrReplace(new DiagnosticsTableRow("memoryused", percentMemUsed.NextValue().ToString()));
                //TableOperation insertCPU = TableOperation.InsertOrReplace(new DiagnosticsTableRow("cpu", procUsed.NextValue().ToString()));

                //diagnosticsTable.Execute(insertAvaliableMem);
                //diagnosticsTable.Execute(insertUsedMem);
                //diagnosticsTable.Execute(insertCPU);

                storageManager.InsertDiagnostic("memoryavaliable", memAvaliable.NextValue().ToString());
                storageManager.InsertDiagnostic("memoryused", percentMemUsed.NextValue().ToString());
                storageManager.InsertDiagnostic("cpu", procUsed.NextValue().ToString());
            }
        }

        private async Task PopulateQueue(int threadNum, CancellationToken cancellationToken)
        {
            string url;
            while (last10UrlConcurrentQueue.TryDequeue(out url) && !cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(50);
                Uri urlParsed = new Uri(url);
                bool fileFormatPass = false;
                if (!urlParsed.AbsolutePath.Contains("."))
                {
                    fileFormatPass = true;
                }
                else if (urlParsed.AbsolutePath.Substring(urlParsed.AbsolutePath.Length - 4) == ".htm")
                {
                    fileFormatPass = true;
                }
                else if (urlParsed.AbsolutePath.Substring(urlParsed.AbsolutePath.Length - 5) == ".html")
                {
                    fileFormatPass = true;
                }

                for (int i = 0; i < baseUrls.Length; i++)
                {
                    if (urlParsed.Host.Contains(baseUrls[i].Host) && fileFormatPass && !UrlAlreadyCrawled(url))
                    {
                        processedUrls.AddOrUpdate(url, true, (key, x) => x);
                        storageManager.InsertToQueue(url, threadNum);
                        //urlQueue.AddMessage(new CloudQueueMessage(url));
                        //InsertQueueLength(threadNum, GetQueueLength(threadNum) + 1);
                    }
                }
            }
        }

        //private int GetQueueLength(int threadNum)
        //{
        //    TableOperation getLength = TableOperation.Retrieve<DiagnosticsTableRow>("diagnostics", "queuelength" + threadNum);
        //    TableResult getLengthResult = diagnosticsTable.Execute(getLength);
        //    if (getLengthResult.Result != null)
        //    {
        //        return int.Parse(((DiagnosticsTableRow)getLengthResult.Result).Value);
        //    }
        //    return 0;
        //}

        //private void InsertQueueLength(int threadNum, int length)
        //{
        //    TableOperation insertQueueLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("queuelength" + threadNum, length.ToString()));
        //    diagnosticsTable.Execute(insertQueueLength);
        //}

        //private int GetTableLength(int threadNum)
        //{
        //    TableOperation getLength = TableOperation.Retrieve<DiagnosticsTableRow>("diagnostics", "tablelength" + threadNum);
        //    TableResult getLengthResult = diagnosticsTable.Execute(getLength);
        //    if (getLengthResult.Result != null)
        //    {
        //        return int.Parse(((DiagnosticsTableRow)getLengthResult.Result).Value);
        //    }
        //    return 0;
        //}

        //private void InsertTableLength(int threadNum, int length)
        //{
        //    TableOperation insertQueueLength = TableOperation.InsertOrReplace(new DiagnosticsTableRow("tablelength" + threadNum, length.ToString()));
        //    diagnosticsTable.Execute(insertQueueLength);
        //}

        private async Task PostLast10(CancellationToken cancellationToken)
        {
            Trace.TraceInformation("PostLast10Manager is starting");
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("PostLast10Manager is running");
                await Task.Delay(20000);

                int i = 1;
                foreach (string url in last10UrlConcurrentQueue.ToArray())
                {
                    storageManager.Last10Insert(url, i);
                    i++;
                }
            }
        }
    }
}
